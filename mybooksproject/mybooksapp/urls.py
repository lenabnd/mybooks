"""mybooksproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from . import views

app_name='mybooksapp'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:booklist_id>/', views.detail_list, name="detail_list"),
    path('detail_genre/<int:list_id>/<int:bookgenre_id>/', views.detail_genre, name="detail_genre"),
    path('detail_book/<int:book_id>/', views.detail_book, name="detail_book"),
    path('new_book', views.new_book, name="new_book"),
    path('delete_item/<int:book_id>/', views.delete_book, name='delete_book'),
    path('update_book/<int:book_id>', views.update_book, name='update_book'),
    path('search', views.search, name="search"),
]