from django.contrib import admin
from .models import BookList, BookGenre, Book

admin.site.register(Book)

class BookInline(admin.TabularInline):
    model = Book

class BookListAdmin(admin.ModelAdmin):
    inlines = [BookInline]

class BookGenreAdmin(admin.ModelAdmin):
    inlines = [BookInline]

admin.site.register(BookList, BookListAdmin)
admin.site.register(BookGenre, BookGenreAdmin)
