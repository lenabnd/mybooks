from django.db import models

class BookList(models.Model):
    name = models.CharField(max_length=200)
    icon = models.CharField(max_length=100, default=True)
    
    def __str__(self):
        return f"{self.name}"

class BookGenre(models.Model):
    name = models.CharField(max_length=200)
    icon = models.CharField(max_length=200, default=True)
    
    def __str__(self):
        return f"{self.name}"

class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    content = models.CharField(max_length=1500)
    rating = models.IntegerField(default=0, null=False)
    done = models.BooleanField(default=False, null=False)
    price = models.DecimalField(max_digits=7, decimal_places=2, default=0.00, null=False)
    cover = models.CharField(max_length=200)
    booklist = models.ForeignKey(BookList, on_delete=models.CASCADE)
    genre = models.ForeignKey(BookGenre, on_delete=models.CASCADE)
    
    def __str__(self):
        return f"{self.title} {self.author} {self.content} {self.rating} {self.done} {self.price} {self.cover} {self.booklist} {self.genre}"